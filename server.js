const puppeteer = require('puppeteer')

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

(async function () {
    const browser = await puppeteer.launch({
        headless: true,
        args: [ // Disable Chromium's unnecessary SUID sandbox.
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ]
    })
    const page = await browser.newPage() // Create a new page instance
    await page.setViewport({ width: 1280, height: 720 })
    await page.goto('http://axiehub.ga/tradingview.html') // Navigate to the pptr.dev website
    await sleep(3000)
    const x = 10
    const y = 10
    const w = 1270
    const h = 710
    await page.screenshot({'path': 'screenshot2.png', 'clip': {'x': x, 'y': y, 'width': w, 'height': h}})
    await console.log('Done')
    await browser.close() // Close the browser
})()